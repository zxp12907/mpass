import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PackFromConfig } from '../packFromConfig.model';
import { EleSelect } from '../eleSelected.model';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'pack-from',
  templateUrl: './pack-from.component.html',
  styleUrls: ['./pack-from.component.scss']
})
export class PackFromComponent implements OnInit {

  @Input()
  public config:PackFromConfig;

  @Output()
  public eleSelected = new EventEmitter<EleSelect>();
  @Output()
  public eleClick =new EventEmitter<string>();

  constructor() { 
    
  }

  ngOnInit() {

  }

  eleClickEvent=()=>{
     
  }

  /**
   * 表单验证
   * 
   * @param {string} name 
   * @returns 
   * @memberof PackFromComponent
   */
  getFormControl(name: string) {
		return this.config.formGroup.controls[name];
  }
  
  /**
   * 表单重置
   * 
   * @memberof PackFromComponent
   */
  resetForm() {
    this.config.formGroup.reset();
  }
}

