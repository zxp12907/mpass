import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PackFromComponent } from './pack-from/pack-from.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgZorroAntdModule } from 'ng-zorro-antd';

@NgModule({
  imports: [
    CommonModule,NgZorroAntdModule, FormsModule,ReactiveFormsModule
  ],
  declarations: [PackFromComponent],
  exports:[PackFromComponent]
})
export class PackageFromModule { }
