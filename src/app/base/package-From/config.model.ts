import { FormGroup } from "@angular/forms";
import { PackFromConfig } from "./packFromConfig.model";
import { PackFormContract } from "./ele.model";

/**
 * 配置实现类
 * 
 * @export
 * @class FromConfig
 * @implements {PackFromConfig}
 */
export class FromConfig implements PackFromConfig {
    formType: "query";
    labelSm: number=6;
    inSm: number=13; 
    formGroup:FormGroup;
    ele: PackFormContract[];
    sc: number=3;//默认显示一行显示
    ns: number=8;
    colns:number=24;

    countNs(c:number) {
        switch(c){
            case 4:this.ns=6;this.colns=24; break;
            case 3:this.ns=8;this.colns=24;break;
            case 2:this.ns=12;this.colns=16;break;
            case 1:this.ns=24;this.colns=10;break;
            default:
            this.ns=8;this.sc=c;break;
        }
    }
}