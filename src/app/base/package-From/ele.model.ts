
/**
 * 字段类型
 * 
 * @export
 * @enum {number}
 */
export enum codeType{
    password,string,number,textarea,select,date,file
}


/**
 * 选择框协议
 * 
 * @export
 * @interface PackFormSelect
 */
export interface PackFormSelect{
    display:string;
    value:any;
}

/**
 * 封装协议
 * 
 * @export
 * @interface PackFormContract
 */
export interface PackFormContract{
   require:boolean; //是否需要*号
   dbName:string;
   desc:string;
   type:codeType;
   selects?:Array<PackFormSelect>;//type 为select 时生
}

export class PackFrom implements PackFormContract {
    need:boolean;//是否有该字段
    require: boolean;//字段是否必填
    dbName: string;
    desc: string;
    type: codeType;
    selects?: PackFormSelect[];
    config:any[] =null;

    constructor(require: boolean,code:string,desc:string,type:codeType,selects?:PackFormSelect[]){
        this.require=require;
        this.dbName=code;
        this.desc=desc;
        this.type=type;
        this.selects=selects;
    }
}


