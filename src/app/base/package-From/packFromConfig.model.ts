
import { FormGroup } from "@angular/forms";
import { PackFormContract } from "./ele.model";


/**
 * 封装表单配置文件
 * 
 * @export
 * @interface PackFromConfig
 */
export interface PackFromConfig{
    formGroup ?: FormGroup;
    ele:Array<PackFormContract>;
    sc:number;
    countNs(c:number);
    labelSm: number;//lable 占多大比重
    inSm: number;//操作框占多大比重
    formType:string;//表单类型  查询表单 提交表单
    ns: number;
    colns:number;
}


