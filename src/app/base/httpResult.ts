export declare class HttpResult<T> extends Response{
    public code:string;
    public data:T;
    public msg;string;
}