import { PackFormSelect } from "../base/package-From/ele.model";


export class LookupValue implements PackFormSelect{
    display:string;
    value:string;
}