export class DbTable{
    public id:number;
    public tableCode:string;
    public tableDesc:string;
    public tableDb:string;
    public createTime:string;
}