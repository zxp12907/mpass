export class DbSource{
    public id:number;
    public name:number;
    public project:string;
    public createTime:string;
    public address:string;
    public port:string;
    public admin:string;
    public password:string;
    public dataSource:string;
    public provider:string;
}