import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class LoadingGlobalService {

  private loadingControlSource= new Subject<boolean>();
  
  public loadingControl=this.loadingControlSource.asObservable();

  public controlLoading(isloading:boolean){
    this.loadingControlSource.next(isloading);
  }
}
