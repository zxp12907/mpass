import { Component } from '@angular/core';
import { LoadingGlobalService } from './service/loading-global/loading-global.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers:[LoadingGlobalService]
})
export class AppComponent {
  public loading=false;

  constructor(private loadingService:LoadingGlobalService){
    this.loadingService.loadingControl.subscribe(isloading=>{
      this.loading=isloading;
    })
  }
}
