import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'com-sider',
  templateUrl: './com-sider.component.html',
  styleUrls: ['./com-sider.component.scss']
})
export class ComSiderComponent implements OnInit {

  public currentNavi = 1;

  constructor(public router: Router) { }

  ngOnInit() {

  }

  naviSelected(navi: number) {
    this.currentNavi = navi;
    if(navi == 0){
      this.router.navigateByUrl("mpass/overview");
    }
    if(navi == 1){
      this.router.navigateByUrl("mpass/dataSource");
    }
    if(navi == 2){
      this.router.navigateByUrl("mpass/meta");
    }
    if(navi == 3){
      this.router.navigateByUrl("mpass/monitor");
    }
  }

}
