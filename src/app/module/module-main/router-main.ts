import { ComMainComponent } from "./component/com-main/com-main.component";


export const routerMain = [
    {
        path: '',
        component: ComMainComponent,
        children: [
            {
                path: 'overview',
                loadChildren: "../module-main/module/module-overview/module-overview.module#ModuleOverviewModule"
            },
            {
                path: 'dataSource',
                loadChildren: "../module-main/module/module-datasource/module-datasource.module#ModuleDatasourceModule"
            },
            {
                path: 'dataSource/table/:id',
                loadChildren: "../module-main/module/module-sqltable/module-sqltable.module#ModuleSqltableModule"
            },
            {
                path: 'dataSource/table/:id/config',
                loadChildren: "../module-config/module-config.module#ModuleConfigModule"
            },
            {
                path:'meta',
                loadChildren:"../module-main/module/module-meta/module-meta.module#ModuleMetaModule"
            },
            {
                path: 'monitor',
                loadChildren: "../module-main/module/module-monitor/module-monitor.module#ModuleMonitorModule"
            }
        ]
    }
]