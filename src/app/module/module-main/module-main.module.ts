import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { routerMain } from './router-main';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComMainComponent } from './component/com-main/com-main.component';
import { ComTopComponent } from './component/com-top/com-top.component';
import { ComSiderComponent } from './component/com-sider/com-sider.component';
import { ComDbsourceComponent } from './component-public/com-dbsource/com-dbsource.component';

@NgModule({
  imports: [
    CommonModule,RouterModule.forChild(routerMain),NgZorroAntdModule,
     FormsModule,ReactiveFormsModule
  ],
  declarations: [ComMainComponent, ComTopComponent, ComSiderComponent]
})
export class ModuleMainModule { }
