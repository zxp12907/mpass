import { DFConfigApi } from './../../../../app.config';
import { DFHttManager } from './../../../../manager/networkManager/network.manager';
import { Observable } from 'rxjs/Observable';
import { PackFrom } from './../../../../base/package-From/ele.model';
import { Injectable } from '@angular/core';

@Injectable()
export class ConfigMetaService {

  constructor(private dFHttManager:DFHttManager) { }

  configMeta(list:Array<PackFrom>):Observable<any>{
    let f=new FieldList();
    f.fieldList=list;
    return this.dFHttManager.post(DFConfigApi.paths.makeForm,f);
  }
}

class FieldList {
  public fieldList:Array<PackFrom>;
}

