import { Observable } from 'rxjs/Rx';
import { Component, OnInit, Input } from '@angular/core';
import { PackFrom } from '../../../../base/package-From/ele.model';
import { rowOptions } from '../../../../config/row-config/row-config';
import { FromConfig } from '../../../../base/package-From/config.model';
import { FormBuilder } from '@angular/forms';
import { ToastService } from '../../../../shared/toast/toast.service';
import { ToastConfig, ToastType } from '../../../../shared/toast/toast-model';
import { ConfigMetaService } from './config-meta.service';

@Component({
  selector: 'com-config-meta',
  templateUrl: './com-config-meta.component.html',
  styleUrls: ['./com-config-meta.component.scss'],
  providers:[ConfigMetaService]
})
export class ComConfigMetaComponent implements OnInit {

  @Input()
  public rowConfig = new Array<PackFrom>();
  public allChecked = false;
  public indeterminate = true;
  public rowColNum = 3;

  public config = new FromConfig();
  public configForm: Array<PackFrom>;

  ishow = false;

  constructor(private fb: FormBuilder,private toastService: ToastService,private configService:ConfigMetaService) { }

  ngOnInit() {
    this.config.formGroup = this.fb.group({})
  }

  formpurpose = [
    { label: '查询', value: 'query', checked: true },
    { label: '提交', value: 'submit', checked: false },
  ];

  formInterface = [
    { label: '查询', value: 'query', checked: true },
    { label: '提交', value: 'submit', checked: false },
    { label: '修改', value: 'update', checked: false },
    { label: '删除', value: 'delete', checked: false },
  ];

  /**
   * 是否全选操作
   * 
   * @memberof ComConfigMetaComponent
   */
  updateAllChecked() {
    this.indeterminate = false;
    Observable.from(this.rowConfig).map(x => {
      if (this.allChecked) {
        x.need = true;
      } else {
        x.need = false;
      }
    }).subscribe(() => {
    })
  }

  /**
   * 表单预览
   * 
   * @memberof ComConfigMetaComponent
   */
  preview = () => {
    this.dealWithData();
    this.config.countNs(this.rowColNum);
  }

  /**
   * 生成表单验证
   * 
   * @memberof ComConfigMetaComponent
   */
  getFb=(p:PackFrom)=>{
    this.config.formGroup.addControl(p.dbName,this.fb.control(''));
  }


  /**
   * 生成表单
   * 
   * @memberof ComConfigMetaComponent
   */
  makeSure(){
    this.dealWithData();
    this.configService.configMeta(this.configForm).subscribe(x=>{
      console.log(x);
    },(e)=>{
      console.log(e);
    })
  }

  dealWithData(){
    this.configForm = new Array<PackFrom>();
    let have = false;
    Observable.from(this.rowConfig).filter(e => e.need).map(x=> {
      have=true;
      this.configForm.push(x);
      this.getFb(x);
    }).subscribe(() => {
      this.config.ele = this.configForm;
    },()=>{},()=>{
      if (!have) {
        const toastCfg = new ToastConfig(ToastType.ERROR, '', '请至少选择一个字段!', 3000);
        this.toastService.toast(toastCfg);
        return
      }
      if (!this.ishow) {
        this.ishow = true;
      }
    })
    console.log(this.configForm);
  }

}
