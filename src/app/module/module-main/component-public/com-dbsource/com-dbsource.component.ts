import { Component, OnInit, Input } from '@angular/core';
import { DbSource } from '../../../../pojo/Mpass-DbSource';
import { Router } from '@angular/router';

@Component({
  selector: 'com-dbsource',
  templateUrl: './com-dbsource.component.html',
  styleUrls: ['./com-dbsource.component.scss']
})
export class ComDbsourceComponent implements OnInit {

  @Input()
  public dbSource=new Array<DbSource>();
  
  constructor(public router: Router) { }

  ngOnInit() {
    
  }

  chooseDb(id:number){
    this.router.navigateByUrl("mpass/dataSource/table/"+id);
  }

}
