import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'com-title',
  templateUrl: './com-title.component.html',
  styleUrls: ['./com-title.component.scss']
})
export class ComTitleComponent implements OnInit {

  @Input()
  public comTitle="";

  constructor() { }

  ngOnInit() {
  }

}
