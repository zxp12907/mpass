import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComConfigMetaComponent } from '../../component-public/com-config-meta/com-config-meta.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PackageFromModule } from '../../../../base/package-From/packageFrom.module';

@NgModule({
  imports: [
    CommonModule,NgZorroAntdModule,
    FormsModule,ReactiveFormsModule,PackageFromModule
  ],
  declarations: [ComConfigMetaComponent],
  exports:[ComConfigMetaComponent]
})
export class ModConfigMetaModule { }
