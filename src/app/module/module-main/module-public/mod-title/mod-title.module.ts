import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComTitleComponent } from '../../component-public/com-title/com-title.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ComTitleComponent],
  exports:[ComTitleComponent]
})
export class ModTitleModule { }
