import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComDbsourceComponent } from '../../component-public/com-dbsource/com-dbsource.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgZorroAntdModule } from 'ng-zorro-antd';

@NgModule({
  imports: [
    CommonModule,NgZorroAntdModule,
    FormsModule,ReactiveFormsModule
  ],
  declarations: [ComDbsourceComponent],
  exports:[ComDbsourceComponent]
})
export class ModSouceModule { }
