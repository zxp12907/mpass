import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComSourceComponent } from './component/com-source/com-source.component';
import { routerDataSource } from './router-datasource';
import { ModTitleModule } from '../../module-public/mod-title/mod-title.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { ModSouceModule } from '../../module-public/mod-souce/mod-souce.module';
import { ComConfigdbComponent } from './component/com-configdb/com-configdb.component';

@NgModule({
  imports: [
    CommonModule,RouterModule.forChild(routerDataSource),ModTitleModule,NgZorroAntdModule,
    FormsModule,ReactiveFormsModule,ModSouceModule
  ],
  declarations: [ComSourceComponent, ComConfigdbComponent]
})
export class ModuleDatasourceModule { }
