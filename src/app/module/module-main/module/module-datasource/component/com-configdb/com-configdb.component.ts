import { Subject } from 'rxjs/Subject';
import { DbSource } from './../../../../../../pojo/Mpass-DbSource';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoadingGlobalService } from '../../../../../../service/loading-global/loading-global.service';
import { ConfigdbService } from './configdb.service';
import { HttpResult } from '../../../../../../base/httpResult';
import { ToastConfig, ToastType } from '../../../../../../shared/toast/toast-model';
import { ToastService } from '../../../../../../shared/toast/toast.service';

@Component({
  selector: 'com-configdb',
  templateUrl: './com-configdb.component.html',
  styleUrls: ['./com-configdb.component.scss'],
  providers:[ConfigdbService]
})
export class ComConfigdbComponent implements OnInit {

  public validateForm: FormGroup;
  public dbSource:DbSource=new DbSource(); 
  public isDisabled=true;
  public inputlistener:Subject<string> =new Subject<string>();
  public page=1;

  public tabs=[
    {
      name:'基础配置',
      page:1
    },{
      name:'连接池',
      page:2
    }
  ]
  
  constructor(private fb: FormBuilder,
    private loadingService:LoadingGlobalService,
    private config:ConfigdbService,private toastService: ToastService) { }

  ngOnInit() {
    this.validateForm = this.fb.group({
      name: [null, [Validators.required]],
      project: [null, [Validators.required]],
      address: [null, [Validators.required]],
      port: [null, [Validators.required]],
      admin: [null, [Validators.required]],
      password: [null, [Validators.required]],
      dataSource: [null, [Validators.required]]
    });
    this.inputlistener
    .debounceTime(200)
    .subscribe(()=>{
      if(this.page == 1){
        this.checkData();
      }
      if(this.page == 2){
        this.checkDataPage();
      }
       
    })
  }

  getFormControl(name) {
    return this.validateForm.controls[name];
  }

  /**
   * tab 选中事件
   * 
   * @param {number} cpage 
   * @memberof ComConfigdbComponent
   */
  tabSelected(cpage:number){
    this.page=cpage;
    if(this.page == 1){
      this.checkData();
    }
    if(this.page == 2){
      this.checkDataPage();
    }
  }

  /**
   * 监听录入
   * 
   * @memberof ComConfigdbComponent
   */
  listener(){
    this.inputlistener.next("str");
  }

  /**
   * 校验form数据
   * 
   * @memberof ComConfigdbComponent
   */
  checkData(){
    var count=0;
    if(this.dbSource.name !=undefined && this.dbSource.name.toString().trim()!=''){
      count++;
    }
    if(this.dbSource.project !=undefined && this.dbSource.project.toString().trim()!=''){
      count++;
    }
    if(this.dbSource.address !=undefined && this.dbSource.address.toString().trim()!=''){
      count++;
    }
    if(this.dbSource.port !=undefined && this.dbSource.port.toString().trim()!=''){
      count++;
    }
    if(this.dbSource.admin !=undefined && this.dbSource.admin.toString().trim()!=''){
      count++;
    }
    if(this.dbSource.password !=undefined && this.dbSource.password.toString().trim()!=''){
      count++;
    }
    if(count === 6){
      this.isDisabled=false;
    }else{
      this.isDisabled=true;
    }
  }

  checkDataPage(){
    var count=0;
    if(this.dbSource.name !=undefined && this.dbSource.name.toString().trim()!=''){
      count++;
    }
    if(this.dbSource.project !=undefined && this.dbSource.project.toString().trim()!=''){
      count++;
    }
    if(this.dbSource.dataSource !=undefined && this.dbSource.dataSource.toString().trim()!=''){
      count++;
    }
    if(count === 3){
      this.isDisabled=false;
    }else{
      this.isDisabled=true;
    }
  }

  /**
   * 数据源操作
   * @param o 
   */
  dbSourceOperator(o:number){
    this.loadingService.controlLoading(true);
    this.config.saveDbConfig(this.dbSource).subscribe((res:HttpResult<any>)=>{
      this.loadingService.controlLoading(false);
      const toastCfg = new ToastConfig(ToastType.SUCCESS, '', '配置数据源成功!', 3000);
      this.toastService.toast(toastCfg);  
    },(error)=>{
      this.loadingService.controlLoading(false);
      const toastCfg = new ToastConfig(ToastType.ERROR, '', '配置数据源信息失败,请重试!', 3000);
      this.toastService.toast(toastCfg);   
    })
  }

}
