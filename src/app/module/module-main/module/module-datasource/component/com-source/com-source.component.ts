import { Observable } from 'rxjs/Rx';
import { DbSource } from './../../../../../../pojo/Mpass-DbSource';
import { Component, OnInit } from '@angular/core';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { ToastConfig, ToastType } from '../../../../../../shared/toast/toast-model';
import { ToastService } from '../../../../../../shared/toast/toast.service';
import { SourceService } from './source.service';
import { HttpResult } from '../../../../../../base/httpResult';
import { trigger, transition, useAnimation } from '@angular/animations';
import { ActivatedRoute } from '@angular/router';
import { comLife } from '../../../../../../animations/common/com-life';

@Component({
  selector: 'com-source',
  templateUrl: './com-source.component.html',
  styleUrls: ['./com-source.component.scss'],
  providers: [SourceService],
  animations: [comLife]
})
export class ComSourceComponent implements OnInit {

  public naviTitle = "数据源列表";
  public dbSourcelist = new Array<DbSource>();
  public current = 0;

  constructor(private loadingBar: LoadingBarService,
    private toastService: ToastService, public sourceService: SourceService) { }

  ngOnInit() {
    this.initData();
  }

  /**
   * 初始化数据源列表
   * 
   * @memberof ComSourceComponent
   */
  initData() {
    this.loadingBar.start();
    this.sourceService.initDbSource().subscribe((res: HttpResult<Array<DbSource>>) => {
      this.dbSourcelist = res.data
      this.loadingBar.complete();
    }, (error) => {
      this.loadingBar.complete();
      const toastCfg = new ToastConfig(ToastType.ERROR, '', '获取数据源列表失败，请重试!', 3000);
      this.toastService.toast(toastCfg);
    })
  }

  /**
   * 配置数据源
   * 
   * @memberof ComSourceComponent
   */
  configDb(c: number) {
    this.current = -1;
    Observable.of(this.current).delay(1000).subscribe(val => {
      if (c === 1) {
        this.naviTitle = "配置数据源"
      }
      if (c === 0) {
        this.naviTitle = "数据源列表"
      }
      this.current = c;
    })
  }

  nSearch(event) {
    
  }

}
