import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { DFHttManager } from '../../../../../../manager/networkManager/network.manager';
import { DFConfigApi } from '../../../../../../app.config';

@Injectable()
export class SourceService {

  constructor(private dFHttManager:DFHttManager) { }

  initDbSource():Observable<any>{
    return this.dFHttManager.get(DFConfigApi.paths.dbSourcelist);
  }

}
