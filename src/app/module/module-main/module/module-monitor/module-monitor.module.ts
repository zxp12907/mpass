import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComMonitorComponent } from './component/com-monitor/com-monitor.component';
import { routerMonitor } from './router-monitor';
import { ModTitleModule } from '../../module-public/mod-title/mod-title.module';
import { ComChartsComponent } from './component/com-charts/com-charts.component';
import { EChartOptionDirective1 } from '../../../../directive/echart-option.directive';

@NgModule({
  imports: [
    CommonModule,RouterModule.forChild(routerMonitor),ModTitleModule
  ],
  declarations: [ComMonitorComponent, ComChartsComponent,EChartOptionDirective1]
})
export class ModuleMonitorModule { }
