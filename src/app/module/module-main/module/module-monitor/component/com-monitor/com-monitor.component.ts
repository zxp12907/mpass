import { Component, OnInit } from '@angular/core';
import { LoadingBarService } from '@ngx-loading-bar/core';

@Component({
  selector: 'com-monitor',
  templateUrl: './com-monitor.component.html',
  styleUrls: ['./com-monitor.component.scss']
})
export class ComMonitorComponent implements OnInit {

  public naviTitle="监控报表";
  constructor(private loadingBar: LoadingBarService) { }

  ngOnInit() {
    this.initData();
  }

  initData(){
    this.loadingBar.start();
    setInterval(()=>{
      this.loadingBar.complete();
    },1000);
  }
}
