import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComOverviewComponent } from './component/com-overview/com-overview.component';
import { routerOverview } from './router-overview';
import { ModTitleModule } from '../../module-public/mod-title/mod-title.module';

@NgModule({
  imports: [
    CommonModule,RouterModule.forChild(routerOverview),ModTitleModule
  ],
  declarations: [ComOverviewComponent]
})
export class ModuleOverviewModule { }
