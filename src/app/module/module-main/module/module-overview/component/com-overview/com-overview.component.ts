import { Component, OnInit } from '@angular/core';
import { LoadingBarService } from '@ngx-loading-bar/core';

@Component({
  selector: 'com-overview',
  templateUrl: './com-overview.component.html',
  styleUrls: ['./com-overview.component.scss']
})
export class ComOverviewComponent implements OnInit {

  public naviTitle="概览";

  constructor(private loadingBar: LoadingBarService) { }

  ngOnInit() {
    this.initData();
  }

  initData(){
    this.loadingBar.start();
    setInterval(()=>{
      this.loadingBar.complete();
    },1000);
  }

}
