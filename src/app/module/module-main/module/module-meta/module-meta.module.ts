import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComMetaComponent } from './component/com-meta/com-meta.component';
import { RouterModule } from '@angular/router';
import { routerMeta } from './router-meta';
import { ModTitleModule } from '../../module-public/mod-title/mod-title.module';

@NgModule({
  imports: [
    CommonModule,RouterModule.forChild(routerMeta),ModTitleModule
  ],
  declarations: [ComMetaComponent]
})
export class ModuleMetaModule { }
