import { Component, OnInit } from '@angular/core';
import { LoadingBarService } from '@ngx-loading-bar/core';

@Component({
  selector: 'com-meta',
  templateUrl: './com-meta.component.html',
  styleUrls: ['./com-meta.component.scss']
})
export class ComMetaComponent implements OnInit {

  public naviTitle="元数据列表";

  constructor(private loadingBar: LoadingBarService) { }

  ngOnInit() {
    this.initData();
  }

  initData(){
    this.loadingBar.start();
    setInterval(()=>{
      this.loadingBar.complete();
    },1000);
  }

}
