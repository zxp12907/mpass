import { Component, OnInit, Input } from '@angular/core';
import { DbSql } from '../../../../../../pojo/Mpass-DbSql';
import { CustomService } from './custom.service';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { ToastService } from '../../../../../../shared/toast/toast.service';
import { ToastConfig, ToastType } from '../../../../../../shared/toast/toast-model';
import { Router } from '@angular/router';
import { LocalStorage } from '../../../../../../app.localStorage';

@Component({
  selector: 'com-custom',
  templateUrl: './com-custom.component.html',
  styleUrls: ['./com-custom.component.scss'],
  providers:[CustomService]
})
export class ComCustomComponent implements OnInit {

  @Input()
  public dataSourceId:string;

  public sqlBean:DbSql=new DbSql();
  public dis=false;
  constructor(private customService:CustomService,
    private toastService: ToastService,
    private loadingBar: LoadingBarService,private router:Router,private ls: LocalStorage) { }

  ngOnInit() {
  }

  connect=()=>{
    this.loadingBar.start();
    this.dis=true;
    this.sqlBean.datasourceId=this.dataSourceId;
    this.customService.connect(this.sqlBean).subscribe(x=>{
      this.loadingBar.complete();
      this.dis=false;
      this.ls.setObject("sqlBean",this.sqlBean);
      this.router.navigateByUrl("mpass/dataSource/table/"+this.dataSourceId+"/config");
    },(e)=>{
      this.loadingBar.complete();
      const toastCfg = new ToastConfig(ToastType.ERROR, '', '连接失败,请重试!', 3000);
      this.toastService.toast(toastCfg);
      this.dis=false;
    })
  }

}
