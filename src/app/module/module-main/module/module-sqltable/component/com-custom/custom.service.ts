import { DFConfigApi } from './../../../../../../app.config';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { DFHttManager } from '../../../../../../manager/networkManager/network.manager';
import { DbSql } from '../../../../../../pojo/Mpass-DbSql';

@Injectable()
export class CustomService {

  constructor(private dFHttManager:DFHttManager) { }

  connect(db:DbSql):Observable<any>{
    return this.dFHttManager.post(DFConfigApi.paths.configField,db);
  }

}
