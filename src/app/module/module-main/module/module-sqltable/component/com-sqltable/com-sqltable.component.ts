import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SqlTableService } from './sql-table.service';
import { DbTable } from '../../../../../../pojo/Mpass-Dbtable';
import { HttpResult } from '../../../../../../base/httpResult';
import { ToastConfig, ToastType } from '../../../../../../shared/toast/toast-model';
import { ToastService } from '../../../../../../shared/toast/toast.service';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { FromConfig } from '../../../../../../base/package-From/config.model';
import { LookupValue } from '../../../../../../pojo/Mpass-lookupValue';
import { DbSource } from '../../../../../../pojo/Mpass-DbSource';
import { PackFrom } from '../../../../../../base/package-From/ele.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'com-sqltable',
  templateUrl: './com-sqltable.component.html',
  styleUrls: ['./com-sqltable.component.scss'],
  providers:[SqlTableService]
})
export class ComSqltableComponent implements OnInit {

  public datasourceId:string;

  public naviTitle = "数据源信息";
  public tables=new Array<DbTable>();
  public dbSourceMsg=new DbSource();//基础配置信息
  public config=new FromConfig();
  public testForm:FormGroup;

  public tabs=[
    {
      name:'结构列表',
      page:1
    },
    {
      name:'自定义',
      page:2
    },{
      name:'基础配置',
      page:3
    },
    
  ]

  constructor(public router: Router,
    private sqlService:SqlTableService,
    private toastService: ToastService,
    private loadingBar: LoadingBarService,
    private fb: FormBuilder,private route: ActivatedRoute,) {
      this.testForm = fb.group({
        id: [null, Validators.required],
        username: [null, Validators.required],
        password: [null, Validators.required],
        code: [null, Validators.required],
        username2: [null, Validators.required],
        password2: [null, Validators.required],
        bri: [null, Validators.required],
        age: [null, Validators.required],
        enter: [null, Validators.required],
      });
      this.config.formGroup=this.testForm;
    }

  ngOnInit() {
    // this.getTablelist();
    this.getDbSourceBasic();
    this.datasourceId=this.route.snapshot.paramMap.get('id');
  }

  /**
   * 返回上一级
   * 
   * @memberof ComSqltableComponent
   */
  goback=()=>{
    this.router.navigateByUrl("mpass/dataSource");
  }

  /**
   * 获取结构列表
   * 
   * @memberof ComSqltableComponent
   */
  getTablelist(){
    this.loadingBar.start();
    this.sqlService.getTableList().subscribe((res:HttpResult<Array<DbTable>>)=>{
      this.loadingBar.complete();
      this.tables=res.data;
    },(error)=>{
      this.loadingBar.complete();
      const toastCfg = new ToastConfig(ToastType.ERROR, '', '获取结构列表失败,请重试!', 3000);
      this.toastService.toast(toastCfg);   
    })
  }

  /**
   * 获取基本配置信息
   * 
   * @memberof ComSqltableComponent
   */
  getDbSourceBasic(){
    this.sqlService.getTableBasic().subscribe((res:HttpResult<DbSource>)=>{
      this.dbSourceMsg=res.data;
    })
  }
}


