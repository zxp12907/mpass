import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { DFHttManager } from '../../../../../../manager/networkManager/network.manager';

@Injectable()
export class SqlTableService {

  constructor(private dFHttManager:DFHttManager) { }

  getTableList():Observable<any>{
    return this.dFHttManager.getTestJson("../../../../../../../assets/json/tableSource.json");
  }

  getTableBasic():Observable<any>{
    return this.dFHttManager.getTestJson("../../../../../../../assets/json/dbBasic.json");
  }

  getTest():Observable<any>{
    return this.dFHttManager.getTestJson("../../../../../../../assets/json/keyTest.json");
  }

  getValue():Observable<any>{
    return this.dFHttManager.getTestJson("../../../../../../../assets/json/KeySelect.json")
  }

}
