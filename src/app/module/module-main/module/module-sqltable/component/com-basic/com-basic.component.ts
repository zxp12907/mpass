import { Component, OnInit, Input } from '@angular/core';
import { DbSource } from '../../../../../../pojo/Mpass-DbSource';

@Component({
  selector: 'com-basic',
  templateUrl: './com-basic.component.html',
  styleUrls: ['./com-basic.component.scss']
})
export class ComBasicComponent implements OnInit {

  @Input()
  public dbMsg=new DbSource();
  
  constructor() { }

  ngOnInit() {
  }

}
