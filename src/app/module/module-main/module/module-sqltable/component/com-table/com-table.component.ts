import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { DbTable } from '../../../../../../pojo/Mpass-Dbtable';

@Component({
  selector: 'com-table',
  templateUrl: './com-table.component.html',
  styleUrls: ['./com-table.component.scss']
})
export class ComTableComponent implements OnInit {

  @Input()
  public dbTables=new Array<DbTable>();

  constructor(private router:Router) { }

  ngOnInit() {
  }

  chooseView=(id:any)=>{
    this.router.navigateByUrl("mpass/dataSource/table/"+id+"/config");
  }
}
