import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComSqltableComponent } from './component/com-sqltable/com-sqltable.component';
import { ComTableComponent } from './component/com-table/com-table.component';
import { ComBasicComponent } from './component/com-basic/com-basic.component';
import { routerSql } from './router-sql';
import { ModTitleModule } from '../../module-public/mod-title/mod-title.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { PackageFromModule } from '../../../../base/package-From/packageFrom.module';
import { ComCustomComponent } from './component/com-custom/com-custom.component';

@NgModule({
  imports: [
    CommonModule,RouterModule.forChild(routerSql),ModTitleModule,NgZorroAntdModule,
    FormsModule,ReactiveFormsModule,PackageFromModule
  ],
  declarations: [ComSqltableComponent, ComTableComponent, ComBasicComponent, ComCustomComponent]
})
export class ModuleSqltableModule { }
