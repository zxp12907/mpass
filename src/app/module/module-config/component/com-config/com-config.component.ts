import { Observable, Scheduler } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ComConfigService } from './com-config.service';
import { HttpResult } from '../../../../base/httpResult';
import { PackFrom } from '../../../../base/package-From/ele.model';
import { rowUpValue } from '../../../../config/row-config/row-config';

@Component({
  selector: 'com-config',
  templateUrl: './com-config.component.html',
  styleUrls: ['./com-config.component.scss'],
  providers:[ComConfigService]
})
export class ComConfigComponent implements OnInit {

  public dataSourceId:string;
  public naviTitle="配置元信息";
  public rowConfig=new Array<PackFrom>();

  constructor(private router:Router,private configSerice:ComConfigService,private route: ActivatedRoute,) { }

  ngOnInit() {
    this.getRowMsg();
    this.dataSourceId=this.route.snapshot.paramMap.get('id');
  }

  goback(){
    this.router.navigateByUrl("mpass/dataSource/table/"+this.dataSourceId);
  }

  getRowMsg(){
    this.configSerice.getTest().subscribe((res:HttpResult<Array<PackFrom>>)=>{
      Observable.from(res.data).map(ele=>{
         if(ele.config === undefined || ele === null){
            ele.config=[];
         };
         if(ele.type!=null){
          ele.config[0]=this.getValueByType(ele.type);
         }
         if(ele.require!=null){
          ele.config[1]=this.getValueByType(ele.require)
         }
      }).observeOn(Scheduler.asap)
      .subscribe(()=>{
        this.rowConfig=res.data;
      })
    })
  }

  /**
   * todo
   * 获取值列表 阿里恶心我
   * 
   * @param {*} type 
   * @returns {string} 
   * @memberof ComConfigComponent
   */
  getValueByType(type:any):string{
    let value="";
    switch(type){
      case "text":value="文本" ;break;
      case "number":value="数字" ;break;
      case "date":value="日期" ;break;
      case "select":value="选择框" ;break;
      case "password":value="密码框" ;break;
      case "true":value="是" ;break;
      case "false":value="否" ;break;
    }
    return value;
  }

}
