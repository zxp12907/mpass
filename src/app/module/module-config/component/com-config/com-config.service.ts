import { Injectable } from '@angular/core';
import { DFHttManager } from '../../../../manager/networkManager/network.manager';
import { Observable } from 'rxjs';
import { DbSql } from '../../../../pojo/Mpass-DbSql';
import { DFConfigApi } from '../../../../app.config';
import { LocalStorage } from '../../../../app.localStorage';

@Injectable()
export class ComConfigService {

  public sqlBean:DbSql=new DbSql();

  constructor(private dFHttManager:DFHttManager,private ls: LocalStorage) { }

  getTest():Observable<any>{
    this.sqlBean=this.ls.getObject("sqlBean")
    return this.dFHttManager.post(DFConfigApi.paths.configField,this.sqlBean);
  }

}
