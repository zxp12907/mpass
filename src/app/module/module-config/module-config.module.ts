import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComConfigComponent } from './component/com-config/com-config.component';
import { RouterModule } from '@angular/router';
import { configRouter } from './router-config';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModTitleModule } from '../module-main/module-public/mod-title/mod-title.module';
import { ModConfigMetaModule } from '../module-main/module-public/mod-config-meta/mod-config-meta.module';

@NgModule({
  imports: [
    CommonModule,RouterModule.forChild(configRouter),NgZorroAntdModule,
    FormsModule,ReactiveFormsModule,ModTitleModule,ModConfigMetaModule
  ],
  declarations: [ComConfigComponent]
})
export class ModuleConfigModule { }
