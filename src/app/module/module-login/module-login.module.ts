import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComLoginComponent } from './com-login/com-login.component';
import { RouterModule } from '@angular/router';
import { loginRouter } from './router-login';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,RouterModule.forChild(loginRouter),NgZorroAntdModule, FormsModule,ReactiveFormsModule
  ],
  declarations: [ComLoginComponent]
})
export class ModuleLoginModule { }
