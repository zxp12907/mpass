import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'com-login',
  templateUrl: './com-login.component.html',
  styleUrls: ['./com-login.component.scss']
})
export class ComLoginComponent implements OnInit {

  public errorMsg='';

  public user={
    'username':"",
    'password':""
  }

  constructor(public router: Router) { }

  ngOnInit() {
  }

  /**
   * 用户登录
   * 
   * @memberof ComLoginComponent
   */
  userlogin=()=>{
    console.log(this.user.username+"密码"+this.user.password);
    if(this.user.username === undefined || this.user.username.trim() === '' 
    || this.user.password === undefined || this.user.password.trim() === ''){
      this.errorMsg="请输入用户名或密码";
      return;
    }
    this.router.navigateByUrl("mpass");
  }
}
