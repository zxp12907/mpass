import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './manager/routerManaer/app.router.module';
import { LocalStorage } from './app.localStorage';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { ToastService } from './shared/toast/toast.service';
import { NetworkModule } from './manager/networkManager/network.module';
import { ToastComponent } from './shared/toast/toast.component';
import { ToastBoxComponent } from './shared/toast/toast-box.component';
import { LoadingModule } from 'ngx-loading';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,ToastBoxComponent,ToastComponent
  ],
  imports: [
    BrowserModule,AppRoutingModule,BrowserAnimationsModule,
    LoadingBarModule.forRoot(),NetworkModule,LoadingModule
  ],
  providers: [LocalStorage,ToastService,{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
