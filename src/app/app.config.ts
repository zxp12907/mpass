/**
 * app 常量配置文件
 */
 export const DFConfigApi:any={
     networkCode:{
         'SUCCESS':'SUCCESS',
         'FAILED':'FAILED'
     },
     domain :'http://10.1.38.58:8000/',//请求域名
     access_token:'1234567890',//OAuth2.0 接口安全验证
     roles:Array<string>(),
     paths:{
        'dbSourcelist':"yaya/api/ds/list",//获取数据源列表
        'configField':"yaya/api/build/field",//配置字段
        'makeForm':"yaya/api/build/commit"//生产表单
     },
     lookupType:{
         'employee_type':'employee_type',
         'educational_bkg':'educational_bkg',
         'certificate_type':'certificate_type'
     }
     ,
     localStorageKey:{
         'userMsg':'userMsg',//用户登录获取的信息
         'token':'token',//token
         'UserBean':'UserBean',//保存用户信息
         'CompanyMsg':'CompanyMsg'//部门列表
     }
 }