import { trigger, style, transition, animate, useAnimation } from "@angular/animations";
import { bounceOutRight } from 'ng-animate';

export const comLife = trigger('comLife', [
    transition("void => *", [
        style({ opacity: 0 }),
        animate(600, style({ opacity: 1 }))
    ]),
    transition('* => void', useAnimation(bounceOutRight))
]);