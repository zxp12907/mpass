export const appRouters=[
    {
        path:'',
        redirectTo:'login',
        pathMatch:'full'
    },
    {
        path:'login',
        loadChildren:"../../module/module-login/module-login.module#ModuleLoginModule"
    },
    {
        path:'mpass',
        loadChildren:"../../module/module-main/module-main.module#ModuleMainModule"
    }
]