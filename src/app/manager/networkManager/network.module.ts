import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule, JsonpModule } from '@angular/http';
import { DFHttManager } from './network.manager';
import { DFHttp } from './netwotk.base';

@NgModule({
  imports: [
    CommonModule,HttpModule,
    JsonpModule
  ],
  declarations: [],
  providers:[DFHttManager,DFHttp]
})
export class NetworkModule { }
