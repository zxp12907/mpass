export const rowOptions = [
    {
        value:"text",
        label:"文本",
        children:[
            {
                value:"true",
                label:"是",
                isLeaf: true
            },
            {
                value:"false",
                label:"否",
                isLeaf: true
            }
        ]
    },
    {
        value:"number",
        label:"数字",
        children:[
            {
                value:"true",
                label:"是",
                isLeaf: true
            },
            {
                value:"false",
                label:"否",
                isLeaf: true
            }
        ]
    },
    {
        value:"date",
        label:"日期",
        children:[
            {
                value:"true",
                label:"是",
                isLeaf: true
            },
            {
                value:"false",
                label:"否",
                isLeaf: true
            }
        ]
    },
    {
        value:"select",
        label:"选择框",
        children:[
            {
                value:"true",
                label:"是",
                isLeaf: true
            },
            {
                value:"false",
                label:"否",
                isLeaf: true
            }
        ]
    },
    {
        value:"password",
        label:"密码框",
        children:[
            {
                value:"true",
                label:"是",
                isLeaf: true
            },
            {
                value:"false",
                label:"否",
                isLeaf: true
            }
        ]
    }
];

export const rowUpValue={
    "text":"文本",
    "number":"数字",
    "date":"日期",
    "select":"选择框",
    "password":"密码框",
    "true":"是",
    "false":"否"
}